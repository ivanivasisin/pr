﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace lab2_pr
{
    class Program
    {
        //Threads
        static Thread thread1;
        static Thread thread2;
        static Thread thread3;
        static Thread thread4;
        static Thread thread5;
        static Thread thread6;

        static AutoResetEvent waitHandler = new AutoResetEvent(false);
        
        //Countdown
        private static readonly CountdownEvent Countdown1 = new CountdownEvent(1);
        private static readonly CountdownEvent Countdown2 = new CountdownEvent(1);
        private static readonly CountdownEvent Countdown3 = new CountdownEvent(1);
        private static readonly CountdownEvent Countdown4 = new CountdownEvent(1);
        

        static void initThreads()
        {
            thread1 = new Thread(firstThread);
            thread2 = new Thread(secondThread);
            thread3 = new Thread(thirdThread);
            thread4 = new Thread(fourThread);
            thread5 = new Thread(fiveThread);
            thread6 = new Thread(sixThread);
            
        }

        static void startThreads()
        {
            thread1.Start();
            thread2.Start();
            thread3.Start();
            thread4.Start();
            thread5.Start();
            thread6.Start();
           
        }


        static void Main(string[] args)
        {
            initThreads();
            startThreads();
            Console.ReadKey();
        }

        static void firstThread()
        {
            Console.WriteLine("Start 1");
            waitHandler.WaitOne();
            Console.WriteLine("Thread 1");
            Countdown1.Signal();
            waitHandler.Set();
            Console.WriteLine("End 1");
        }
        static void secondThread()
        {
            Console.WriteLine("Start 2");
            Countdown1.Wait();
            waitHandler.WaitOne();
            Console.WriteLine("Thread 2");
            Countdown2.Signal();
            waitHandler.Set();
            Console.WriteLine("End 2");
        }
        static void thirdThread()
        {
            Console.WriteLine("Start 3");
            Console.WriteLine("Thread 3");
            waitHandler.Set();
            Console.WriteLine("End 3");
        }
        static void fourThread()
        {
            Console.WriteLine("Start 4");
            Countdown3.Wait();
            waitHandler.WaitOne();
            Console.WriteLine("Thread 4");
            Countdown4.Signal();
            waitHandler.Set();
            Console.WriteLine("End 4");
        }
        static void fiveThread()
        {
            Console.WriteLine("Start 5");
            Countdown2.Wait();
            waitHandler.WaitOne();
            Console.WriteLine("Thread 5");
            Countdown3.Signal();
            waitHandler.Set();
            Console.WriteLine("End 5");
        }
        static void sixThread()
        {
            Console.WriteLine("Start 6");
            Countdown4.Wait();
            waitHandler.WaitOne();
            Console.WriteLine("Thread 6");
            Console.WriteLine("End 6");

        }
    }
}
